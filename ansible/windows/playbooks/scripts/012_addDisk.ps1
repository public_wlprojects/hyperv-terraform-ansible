# dodaj dysk d
Write-Host "Adding disk E:"
#Get-Disk 
$diskNumber = (Get-Disk).Number
$partitionNumber = (Get-Partition | Where-Object DriveLetter -like "C").PartitionNumber
$partitionSize = (Get-Partition | Where-Object DriveLetter -like "C").Size
$diskDSize = $partitionSize/2
Resize-Partition -DiskNumber $diskNumber -PartitionNumber $partitionNumber -Size $($partitionSize-$diskDSize)
New-Partition -DiskNumber $diskNumber -Size $diskDSize -DriveLetter E
Format-Volume -DriveLetter E -FileSystem NTFS -Force

Get-Volume
