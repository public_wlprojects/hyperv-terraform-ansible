$interfaceIndexNumer = (Get-NetIPAddress | Select-Object AddressFamily, PrefixLength, InterfaceIndex  | Where-Object {$_.AddressFamily -like "IPv4" -and $_.PrefixLength -NE 20 -and $_.InterfaceIndex -ne 1}).InterfaceIndex

New-NetIPAddress -IPAddress 10.100.100.94 -PrefixLength 24 -DefaultGateway 10.100.100.254 -InterfaceIndex $interfaceIndexNumer

Set-DnsClientServerAddress -InterfaceIndex $interfaceIndexNumer -ServerAddresses ('10.100.100.94','8.8.8.8')

Get-DnsClientServerAddress
Get-NetIPAddress