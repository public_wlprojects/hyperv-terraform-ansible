Write-Host "Verify source file..."

#variables
$savePath = "C:\temp"
$ProgressPreference = 'SilentlyContinue'

# verify path exist
if (Test-Path -Path $savePath) {
    Write-Host "Path exist ====>"
} else {
    Write-Host "Creating temp directory"
    New-Item -Path "C:\" -Name "temp" -ItemType Directory    
}

Write-Host "Sleep 3s"
Start-Sleep 3

$files = @(
    @{
        Uri = "https://www.7-zip.org/a/7z2201-x64.exe"
        OutFile = '7z2201-x64.exe'
    },
    @{
        Uri = "https://github.com/notepad-plus-plus/notepad-plus-plus/releases/download/v8.4.8/npp.8.4.8.Installer.x64.exe"
        OutFile = 'npp.8.4.8.Installer.x64.exe'
    },    
    @{
        Uri = "https://github.com/git-for-windows/git/releases/download/v2.39.1.windows.1/Git-2.39.1-64-bit.exe"
        OutFile = 'Git-2.39.1-64-bit.exe'
    },
    @{
        Uri = "https://download-installer.cdn.mozilla.net/pub/firefox/releases/101.0.1/win64/en-US/Firefox%20Setup%20101.0.1.exe"
        OutFile = 'FirefoxSetup101.0.1.exe'
    },
    @{
        Uri = "http://pliki.totalcmd.pl/pobierz.php?typ=app&plik=tcmd1052x64.exe"
        OutFile = 'tcmd1052x64.exe'
    },
    @{
        Uri = "https://github.com/WinMerge/winmerge/releases/download/v2.16.26/WinMerge-2.16.26-x64-Setup.exe"
        OutFile = 'WinMerge-2.16.26-x64-Setup.exe'
    },
    @{
        Uri = "https://az764295.vo.msecnd.net/stable/abd2f3db4bdb28f9e95536dfa84d8479f1eb312d/VSCodeSetup-x64-1.82.2.exe"
        OutFile = 'VSCodeSetup-x64-1.82.2.exe'
    }
)

Write-Host "Downloading applications..."
foreach ($file in $files){
    Write-Host "====> $($file.OutFile)"
    Invoke-WebRequest -Uri "$($file.Uri)" -OutFile "$savePath\$($file.OutFile)"
}

Write-Host "Sleep 3s"
Start-Sleep 3

Write-Host "Installing applications..."

    Set-Location "$savePath"

    Write-Host "========> 7z2201-x64.exe"
    Start-Process -FilePath 7z2201-x64.exe -Args "/S" -Verb RunAs -Wait
    Write-Host "========> npp.8.4.8.Installer.x64.exe"
    Start-Process -FilePath npp.8.4.8.Installer.x64.exe -ArgumentList "/S" -Verb RunAs -Wait
    Write-Host "========> Git-2.39.1-64-bit.exe"
    Start-Process -FilePath Git-2.39.1-64-bit.exe -ArgumentList "/VERYSILENT /NORESTART" -Verb RunAs -Wait
    Write-Host "========> WinMerge-2.16.26-x64-Setup.exe"
    Start-Process -FilePath WinMerge-2.16.26-x64-Setup.exe -ArgumentList "/VERYSILENT /NORESTART" -Verb RunAs -Wait
    Write-Host "========> Total Commander"
    Start-Process -FilePath tcmd1052x64.exe -ArgumentList "/AHMGDU" -Verb RunAs -Wait
    Write-Host "========> Firefox Setup 101.0.1.exe"
    Start-Process -FilePath FirefoxSetup101.0.1.exe -ArgumentList "/S" -Verb RunAs -Wait
    Write-Host "========> VSCodeSetup-x64-1.82.2.exe"
    Start-Process -FilePath VSCodeSetup-x64-1.82.2.exe -ArgumentList "/VERYSILENT /NORESTART /MERGETASKS=!runcode" -Verb RunAs -Wait
