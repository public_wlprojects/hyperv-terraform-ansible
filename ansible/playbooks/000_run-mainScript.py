
import json
import os
import subprocess

#variables
currentWorkDir = os.getcwd() 
os.chdir('../../')
configFilePathDirectory = os.getcwd()
configFilePathFull = "".join(configFilePathDirectory+'/config.json') 

with open(configFilePathFull,'r') as json_File :
    sample_load_file=json.load(json_File)

# set correct path
os.chdir(currentWorkDir)

# run script 003 
print("Run playbook 003-installUpdates.yml")
subprocess.run(["ansible-playbook", "./003-installUpdates.yml"])    

# run script 001
print("Run playbook 001-renameHostName.yml")
for machine in sample_load_file['config']['100']['options']:
    print("Configure machine {}".format(machine['VM_Name']))
    subprocess.run(["ansible-playbook", "--extra-vars", "pwd_path={} host_name={} ip_address={}".format(currentWorkDir, machine['VM_Name'], machine['VM_IP']), "./001-renameHostName.yml"])


# run script 002 
print("Run playbook 002-installApp01.yml")
subprocess.run(["ansible-playbook", "--extra-vars", "pwd_path={}".format(currentWorkDir), "./002-installApp01.yml"])

# # run script 004 
# print("Run playbook 002-installApp01.yml")
# subprocess.run(["ansible-playbook", "--extra-vars", "pwd_path={}".format(currentWorkDir), "./002-installApp01.yml"])

