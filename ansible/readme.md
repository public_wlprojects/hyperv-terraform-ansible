### install control node

You cannot use a Windows system for the Ansible control node. Use Linux instead of it.

Linux: https://docs.ansible.com/ansible/latest/installation_guide/installation_distros.html#installing-ansible-on-ubuntu

### set up client

When use a packer run script 012_ConfigureRemotingForAnsible.ps1 in oobe sysprep. 

### commands

`ansible windows -m setup -i hosts.ini` - simple command on windows group machines

### run playbook

`ansible-playbook playbooks/new_hostname.yml -i hosts.ini` - run playbook