variable "sandboxes" {
  type = map(object({
    machine_name = string,
    vhd_path = string
  }))
      default = {
      sandbox_0 = {
        machine_name = "VM60"
        vhd_path = "C:\\temp\\dev_terraform\\VM60\\VM60.vhdx"
      },
      sandbox_1 = {
        machine_name = "VM61"
        vhd_path = "C:\\temp\\dev_terraform\\VM61\\VM61.vhdx"
      },
      sandbox_2 = {
        machine_name = "VM62"
        vhd_path = "C:\\temp\\dev_terraform\\VM62\\VM62.vhdx"
      },
      sandbox_3 = {
        machine_name = "VM63"
        vhd_path = "C:\\temp\\dev_terraform\\VM63\\VM63.vhdx"
      },
      sandbox_4 = {
        machine_name = "VM64"
        vhd_path = "C:\\temp\\dev_terraform\\VM64\\VM64.vhdx"
      },
      sandbox_5 = {
        machine_name = "VM65"
        vhd_path = "C:\\temp\\dev_terraform\\VM65\\VM65.vhdx"
      }
    }
}

variable "switch_name" {
  type = map
  default = {
    "Default"  = "Default Switch"
    "New"  = "Net61"
  }
}


















