variable "sandboxes" {
  type = map(object({
    machine_name = string,
    vhd_path = string
  }))
  default = {
    sandbox_0 = {
      machine_name = "VM60"
      vhd_path = "D:\\Hyper-V\\dev_terraform\\VM60\\VM60.vhdx"
    },
    sandbox_1 = {
      machine_name = "VM61"
      vhd_path = "D:\\Hyper-V\\dev_terraform\\VM61\\VM61.vhdx"
    },
    sandbox_2 = {
      machine_name = "VM62"
      vhd_path = "D:\\Hyper-V\\dev_terraform\\VM62\\VM62.vhdx"
    }
  }
}

variable "switch_name" {
  type = map
  default = {
    "Default"  = "Default Switch"
    "New"  = "Net61"
  }
}