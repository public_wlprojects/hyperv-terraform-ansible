### Wersja

`terraform --version`

Jeśli za stara to pobierz z lokalizacji: https://www.terraform.io/downloads.html
Windows 10 - **wersja windows_amd64**

### Konfiguracja

Pobierz zip i rozpakuj do katalogu np: C:\Program Files\terraform

Trzeba dodoać do zmiennych środowiskowych ścieżkę do pliku w sekcji Path.

### Provisioniung 

`terraform init`

