# [CmdletBinding()]
# Param(
#     [Parameter(Position=0,mandatory=$true)]
#     [string]$userName,
#     [Parameter(Position=1,mandatory=$true)]
#     [string]$password
#     )
# here main script to setup machine

Write-Host "Start main script..." -ForegroundColor Blue

Write-Host "[STEP1] - prepare disks for HyperV machines..." -ForegroundColor Magenta
Set-Location $PSScriptRoot\powershell
.\01_prepareDisksAndVmPaths.ps1

Write-Host "[STEP2] - prepare variables.tf file..." -ForegroundColor Magenta
Set-Location $PSScriptRoot\powershell
.\03_setVariablesTfFile.ps1

Write-Host "[STEP3] - Terraform prepares a machines in hyperV..." -ForegroundColor Magenta
Set-Location $PSScriptRoot\terraform
terraform init
terraform plan --out=tfplan
#terraform apply tfplan
#$process_terraform = Start-Process terraform -ArgumentList "apply tfplan -var terrUser=$userName -var terrPass=$password" -Wait -PassThru -NoNewWindow
$process_terraform = Start-Process terraform -ArgumentList "apply tfplan" -Wait -PassThru -NoNewWindow
if ($($process_terraform.ExitCode) -eq 0){
    Write-Host "Correct Install" -ForegroundColor Green
}
else {
    Write-Host "Exit Code: $($process_terraform.ExitCode)"
    exit
    }

Write-Host "Wait 30 seconds..."
Start-Sleep 30

Write-Host "[STEP4] - Set static ip for network Netxx..." -ForegroundColor Magenta
Set-Location $PSScriptRoot\powershell
.\02_setStaticIP.ps1