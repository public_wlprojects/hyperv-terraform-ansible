## Project description

Deploy environment for test.local domain.

 * vm60-win2019 - vm60 - domain controller - 10.0.51.60
 * vm61-win2012R2 - vm61 - web server - 10.0.51.61
 * vm62-win2019 - vm62 - db server - 10.0.51.62
 * vm63-win10 - vm63 - client win10 - 10.0.51.63

Network - Net51:
 * 10.0.51.0/24

## How run

1. Configure .\config.json file.
2. Run .\setup_machine.ps1 script 
3. Edit hosts i Ansible Host machine
4. Run script ansible\playbooks\000_run-mainScript.py
