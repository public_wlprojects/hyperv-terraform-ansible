# Run this script on virtual machine

#support functions
function get-temporaryAdapterIP {
    Param(
        $vm_name
        )
        $networAdaptersList = (Get-VM | Where-Object Name -like $vm_name).NetworkAdapters.IPAddresses
        $defaultSwitchIP = $networAdaptersList.Where({$_ -like "172.*"}) 

        return $defaultSwitchIP
}

function test-winRmTestConnection {
    Param(
        $ip_address,
        $ip__address,
        $ip__gateway,
        $ip__dns,
        $adapter__macAddress,
        $user__name,
        $user__password
    )
    for ($num = 1 ; $num -le 10 ; $num++){
        Write-Host "Testing connection. Repeat ${num}/10..."
        $respond = (Test-Netconnection $ip_address -port 5986).TcpTestSucceeded
        Start-Sleep -Seconds 10
        if ($respond -eq "True"){
            Write-Host "Test okay for $ip_address"
            connect-toRemoteMachine $defaultSwitchIPAddress $ip__address $ip__gateway $ip__dns $adapter__macAddress $user__name $user__password
            break
        }else {
            Write-Host "Not connected"
        } 
    }
}


function get-macAddress {
    Param(
        $vm_name,
        $adapter_name
        )
        $networAdaptersList = (Get-VM | Where-Object Name -like $vm_name).NetworkAdapters
        $adapterMACAddress = ((($networAdaptersList.Where({$_.Name -like $adapter_name})).MacAddress -split '([A-Z0-9]{2})' -join '-').Trim('-')).Replace('--','-') 

        return $adapterMACAddress
}

function connect-toRemoteMachine {
    Param(
        $ip_address,
        $ip__address,
        $ip__gateway,
        $ip__dns,
        $adapter_macAddress,
        $user_name,
        $user_password
        )
        #Set credentials needed for remote installation
        $userName = $user_name
        $password = ConvertTo-SecureString $user_password -AsPlainText -Force
        $cred = New-Object System.Management.Automation.PSCredential -ArgumentList ($userName, $password)
        #Add specific computer to TrustedHosts list
        #Set-Item -Path WSMan:\localhost\Client\TrustedHosts -Value "$ip_address" -Concatenate -Force
        Write-Host "@@@@@@ Connecting remotely to $ip_address"
        $session = New-PSSession -ComputerName $ip_address -Credential $cred
        Write-Host "Invoking command remotely to $ip_address / $adapter_macAddress"
        Invoke-Command -Session $session -ScriptBlock ${Function:set-staticIP} -ArgumentList "$ip__address", "$ip__gateway", "$ip__dns", "$adapter_macAddress"
        Write-Host "@@@@@@@@@ Removing remote session to $ip_address"
        Write-Host ""
        Remove-PSSession -Session $session
}


function set-staticIP {
    Param(
        $ip_address,
        $ip_gateway,
        $ip_dns,
        $mac_address
        )

    $ipInterfaceIndex = (Get-NetAdapter | Where-Object MacAddress -like "$mac_address").InterfaceIndex

    Write-Host "Configuring static IP $ip_address on virtual machine" 
    New-NetIPAddress -IPAddress $ip_address -DefaultGateway $ip_gateway -PrefixLength 24 -InterfaceIndex $ipInterfaceIndex
    #Add DNS to interface
    Write-Host "Configuring DNS on virtual machine"
    Set-DNSClientServerAddress -InterfaceIndex $ipInterfaceIndex -ServerAddresses $ip_dns
}

Set-Location $PSScriptRoot
Set-Location ..
$configPathFile = ".\config.json"
$sourceContent = Get-Content -Raw -Path $configPathFile | ConvertFrom-Json

foreach($elem in $sourceContent.config.'100'.options){

    Write-Host "@@@ Configuring $($elem.VM_Name) - $($elem.VM_DESC)..."
    $userName = $sourceContent.config.'105'.options.USER_NAME
    $pasword = $sourceContent.config.'105'.options.USER_PASSWORD
    $defaultSwitchIPAddress = get-temporaryAdapterIP $($elem.VM_Name)
    $adapterMacAddress = get-macAddress $($elem.VM_Name) $($elem.VM_SwitchName)
    test-winRmTestConnection $defaultSwitchIPAddress $($elem.VM_IP) $($elem.VM_IP_Gateway) $($elem.VM_IP_DNS) $adapterMacAddress $userName $pasword
}
