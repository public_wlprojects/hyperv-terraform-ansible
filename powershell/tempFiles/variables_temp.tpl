variable "sandboxes" {
  type = map(object({
    machine_name = string,
    vhd_path = string
  }))
  <placeHere>
}

variable "switch_name" {
  type = map
  default = {
    "Default"  = "Default Switch"
    "New"  = "Net61"
  }
}