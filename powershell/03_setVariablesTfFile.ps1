Set-Location $PSScriptRoot
Set-Location ..
$configPathFile = ".\config.json"
$sourceContent = Get-Content -Raw -Path $configPathFile | ConvertFrom-Json
$elemCount = 0
$vmDiskPath = $sourceContent.config.'110'.options.'VM_Disk_Path'

function set-template {
    Param(
        $sundbox_number,
        $vm_name,
        $vm_sourceGold
        )
    $vm_sourceGoldYaml = "$vm_sourceGold\$vm_name\$vm_name.vhdx".Replace('\','\\')
    (Get-Content -Path ".\powershell\tempFiles\variables_temp.tf" -Raw) -replace "<sandboxNumber$($sundbox_number)>","$($sundbox_number)" | Set-Content ".\powershell\tempFiles\variables_temp.tf"
    (Get-Content -Path ".\powershell\tempFiles\variables_temp.tf" -Raw) -replace "<vmName$($sundbox_number)>","$($vm_name)" | Set-Content ".\powershell\tempFiles\variables_temp.tf"
    (Get-Content -Path ".\powershell\tempFiles\variables_temp.tf" -Raw) -replace "<vmPath$($sundbox_number)>","$($vm_sourceGoldYaml)" | Set-Content ".\powershell\tempFiles\variables_temp.tf"

}        
    
function apply-template {
    Param(
        $template_number
        )

    (Get-Content -Path ".\powershell\tempFiles\variables_temp.tpl" -Raw) -replace "<placeHere>","$($template_number)" | Set-Content .\powershell\tempFiles\variables_temp.tf
}

# 1 - apply template
$template_number = ($sourceContent.config.'100'.options).Count -1
Write-Host "Preparing template number: $template_number"

if ( $template_number -eq 0) {

    $template_0 = @"
    default = {
      sandbox_<sandboxNumber0> = {
        machine_name = "<vmName0>"
        vhd_path = "<vmPath0>"
      }
    }
"@

  apply-template $template_0

}
    # templates
if ( $template_number -eq 1) {

    $template_1 = @"
    default = {
      sandbox_<sandboxNumber0> = {
        machine_name = "<vmName0>"
        vhd_path = "<vmPath0>"
      },
      sandbox_<sandboxNumber1> = {
        machine_name = "<vmName1>"
        vhd_path = "<vmPath1>"
      }
    }
"@

  apply-template $template_1

}

if ( $template_number -eq 2) {

    $template_2 = @"
default = {
    sandbox_<sandboxNumber0> = {
      machine_name = "<vmName0>"
      vhd_path = "<vmPath0>"
    },
    sandbox_<sandboxNumber1> = {
      machine_name = "<vmName1>"
      vhd_path = "<vmPath1>"
    },
    sandbox_<sandboxNumber2> = {
      machine_name = "<vmName2>"
      vhd_path = "<vmPath2>"
    }
  }
"@

  apply-template $template_2

}

if ( $template_number -eq 3) {
    $template_3 = @"
    default = {
      sandbox_<sandboxNumber0> = {
        machine_name = "<vmName0>"
        vhd_path = "<vmPath0>"
      },
      sandbox_<sandboxNumber1> = {
        machine_name = "<vmName1>"
        vhd_path = "<vmPath1>"
      },
      sandbox_<sandboxNumber2> = {
        machine_name = "<vmName2>"
        vhd_path = "<vmPath2>"
      },
      sandbox_<sandboxNumber3> = {
        machine_name = "<vmName3>"
        vhd_path = "<vmPath3>"
      }
    }
"@

apply-template $template_3

}

if ( $template_number -eq 4) {

    $template_4 = @"
    default = {
      sandbox_<sandboxNumber0> = {
        machine_name = "<vmName0>"
        vhd_path = "<vmPath0>"
      },
      sandbox_<sandboxNumber1> = {
        machine_name = "<vmName1>"
        vhd_path = "<vmPath1>"
      },
      sandbox_<sandboxNumber2> = {
        machine_name = "<vmName2>"
        vhd_path = "<vmPath2>"
      },
      sandbox_<sandboxNumber3> = {
        machine_name = "<vmName3>"
        vhd_path = "<vmPath3>"
      },
      sandbox_<sandboxNumber4> = {
        machine_name = "<vmName4>"
        vhd_path = "<vmPath4>"
      }
    }
"@

  apply-template $template_4

}

if ( $template_number -eq 5) {

    $template_5 = @"
    default = {
      sandbox_<sandboxNumber0> = {
        machine_name = "<vmName0>"
        vhd_path = "<vmPath0>"
      },
      sandbox_<sandboxNumber1> = {
        machine_name = "<vmName1>"
        vhd_path = "<vmPath1>"
      },
      sandbox_<sandboxNumber2> = {
        machine_name = "<vmName2>"
        vhd_path = "<vmPath2>"
      },
      sandbox_<sandboxNumber3> = {
        machine_name = "<vmName3>"
        vhd_path = "<vmPath3>"
      },
      sandbox_<sandboxNumber4> = {
        machine_name = "<vmName4>"
        vhd_path = "<vmPath4>"
      },
      sandbox_<sandboxNumber5> = {
        machine_name = "<vmName5>"
        vhd_path = "<vmPath5>"
      }
    }
"@

  apply-template $template_5

}
  
#2 - set template
foreach($elem in $sourceContent.config.'100'.options){
    
    set-template $elemCount $($elem.VM_Name) $vmDiskPath
    $elemCount++
}

#3 - copy file to terraform directory
Move-Item -Path ".\powershell\tempFiles\variables_temp.tf" -Destination ".\terraform\variables.tf" -Force
