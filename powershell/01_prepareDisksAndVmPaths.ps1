


#functions
function new-diskForVM {
    Param(
        $vm_mainPathVHDFile,
        $vm_name,
        $vm_sourceGold
        )

    if(!(Test-Path "$vm_mainPathVHDFile\$vm_name")){
        Write-Host "Copy files for $vm_name... $vm_sourceGold => $vm_mainPathVHDFile\$vm_name" 
        New-Item -Path "${mainPath}\" -Name $vm_name -ItemType "directory" 
        Copy-Item $vm_sourceGold -Destination "$vm_mainPathVHDFile\${vm_name}\${vm_name}.vhdx"
        Write-Host "OK" -ForegroundColor Green
    }else {
        Write-Host "Skipping $vm_name"
    }

}

# main part
Set-Location $PSScriptRoot
Set-Location ..
$configPathFile = ".\config.json"
$sourceContent = Get-Content -Raw -Path $configPathFile | ConvertFrom-Json

#variables
$mainPath = $sourceContent.config.'110'.options.VM_Disk_Path

foreach($elem in $sourceContent.config.'100'.options){

    Write-Host "Preparing disk for: $($elem.VM_Name) - $($elem.VM_Type) - $($elem.VM_DESC)"
    new-diskForVM $mainPath $($elem.VM_Name) $($elem.VM_Disk)
    
}
